package equipamentos;


public class Equipamento implements Cloneable {
	
	protected int patrimonio;
	protected int centroCusto;
	protected String nome;
	protected String descricao;
	
	
	
	public void setPatrimonio(int p)throws Exception 
	{
		if(p <= 0)
			throw new Exception ("Erro: Equipamento deve ter patrimonio valido");
		
		this.patrimonio = p;
		
	}
	public void setCentroCusto(int c)throws Exception 
	{
		if(c <= 0)
			throw new Exception ("Erro: Equipamento deve ter um centro de custo valido");
		
		this.centroCusto = c;
		
	}
	public int getPatrimonio()
	{
		return this.patrimonio;
	}
	
	public int getCentroCusto()
	{
		return this.centroCusto;
	}
	
	public Equipamento(int patri, int cCusto)throws Exception
	{
		this.setPatrimonio(patri);
		this.setCentroCusto(cCusto);
	}
	
	public int equals(Equipamento obj)
	{
		//implementar esse metodo canonico
		return 1;
	}
	
	public String toString()
	{
		String ret="";
		//implementar esse metodo canonico
		return ret;
	}
	
	public int hashCode()
	{
		int ret = super.hashCode();
		
		//implementar esse metodo canonico
		return ret;
	}
	
	public Equipamento(Equipamento modelo)throws Exception
	{
		//implementar esse metodo canonico
		
	}
	public Equipamento clone()
	{
		Equipamento ret = null;
		
		try{
			
			ret = new Equipamento(this);
		}
		catch (Exception e) {
		}
		
		return ret;
	}
	
	
}
