package janelas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Event;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.CardLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EtchedBorder;
import javax.swing.border.CompoundBorder;

public class principal extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	
	protected int patrimonio;
	protected boolean buscaUnica;
	protected JLabel lblNewLabel;
	protected JButton btnBuscar ;
	protected JRadioButton rdbtnTodos;
	protected JRadioButton rdbtnPatrimonio;
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel label_3;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					principal frame = new principal();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public principal() {
		initialize();
	}
	

	private void initialize()
	{
		setTitle("Mapa de Equipamentos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 365, 171);
		contentPane = new JPanel();
		contentPane.setBorder(new CompoundBorder());
		setContentPane(contentPane);
		
		lblNewLabel = new JLabel("Consulta:");
		lblNewLabel.setBackground(Color.DARK_GRAY);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		textField = new JTextField();
		textField.setToolTipText("Digite aqui o patrimonio do equipamento");
		textField.setColumns(10);
		this.buscaUnica = true;
		
		ButtonGroup group = new ButtonGroup();
		
		label = new JLabel("");
		
		rdbtnPatrimonio = new JRadioButton("Patrimonio");
		rdbtnPatrimonio.setSelected(true);
		rdbtnPatrimonio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setBuscaUnica(true);
				textField.setEnabled(true);
			}
		});
		group.add(rdbtnPatrimonio);
		
	    rdbtnTodos = new JRadioButton("Todos");
	    rdbtnTodos.setSelected(false);
	    rdbtnTodos.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		setBuscaUnica(false);
	    		textField.setEnabled(false);
	    		limparCaixaTexto();
	    	}
	    });
	    group.add(rdbtnTodos);
		
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				limparCaixaTexto();
			}
		});
		
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					
			}
		});
		
		label_1 = new JLabel("");
		
		label_2 = new JLabel("");
		
		label_3 = new JLabel("");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(19)
					.addComponent(lblNewLabel)
					.addGap(5)
					.addComponent(textField)
					.addGap(5)
					.addComponent(label)
					.addGap(5)
					.addComponent(rdbtnPatrimonio, GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)
					.addGap(5)
					.addComponent(rdbtnTodos, GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)
					.addGap(24))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(98)
					.addComponent(btnBuscar)
					.addGap(5)
					.addComponent(btnLimpar)
					.addGap(5)
					.addComponent(label_2))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(2)
							.addComponent(lblNewLabel))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(1)
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(11)
							.addComponent(label))
						.addComponent(rdbtnPatrimonio)
						.addComponent(rdbtnTodos))
					.addGap(5)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnBuscar)
						.addComponent(btnLimpar)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(11)
							.addComponent(label_2))))
		);
		contentPane.setLayout(gl_contentPane);
	}
	
	protected void setPatrimonio(int patri)
	{
		this.patrimonio = patri;
	}
	
	protected void setBuscaUnica(boolean b )
	{
		this.buscaUnica = b;
	}
	protected void limparCaixaTexto()
	{
		textField.setText("");
	}
	
		
	
	
}
