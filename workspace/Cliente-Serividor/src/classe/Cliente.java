package classe;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class Cliente extends Thread{

	private Socket servidor;
	
	public Cliente(Socket servidor)
	{
		this.servidor = servidor;
	}
	
	public static void main(String[] args)
	{
		try{
			Socket servidor = new Socket("localhost",10001);
			
			Thread escuta = new Cliente(servidor);
			
			escuta.start();
			
			Scanner leitor = new Scanner(System.in);
			PrintStream saida = new PrintStream(servidor.getOutputStream());
			
			while(leitor.hasNextLine())
			{
				String mensagem = leitor.nextLine();
				
				System.out.println("Dado enviado: " + mensagem);
				saida.println(mensagem);
				
			}
		}catch(IOException e)
		{
			
		}
	}
	public void run()
	{
		try{
			Scanner entrada = new Scanner(this.servidor.getInputStream());
			
			while(entrada.hasNextLine())
			{
				System.out.println("Dado Recebido: " + entrada.nextLine());
			}
			
			
			
		}catch(IOException e)
		{
			
		}
	}
	
}
