package equipamentos;

public class Microfone extends Equipamento {

	protected String modelo;
	protected String descricao;
	
	public void setModelo(String name)throws Exception 
	{
		if(name == null)
			throw new Exception("Erro: Microfone n�o pode ter modelo nulo");
		
		this.modelo = name;	
	}
	
	public void setDescricao(String descri)throws Exception 
	{
		if(descri == null)
			throw new Exception("Erro: Microfone n�o pode ter descricao nulo");
		
		this.descricao = descri;	
	}
	public String getModelo()
	{
		return this.modelo;
			
	}
	public String getDescricao()
	{
		if(this.descricao != null)
				return this.descricao;
		
		return " -";	
	}
	
	
	
	public Microfone(int patri, int cCusto,String model , String descri) throws Exception {
		super(patri, cCusto);
		setModelo(model);
		setDescricao(descri);
		// TODO Auto-generated constructor stub
	}

	public Microfone(Equipamento modelo) throws Exception {
		super(modelo);
		// TODO Auto-generated constructor stub
	}

}
