package janelas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Janela {

	private JFrame frmCalculadora;
	protected char operador = ' ';
	protected double operando1;
	protected boolean acrescenta = false;
	
	protected JLabel lblVisor = new JLabel("0");
	protected JButton btn0 = new JButton("0");
	protected JButton btn1 = new JButton("1");
	protected JButton btn2 = new JButton("2");
	protected JButton btn3 = new JButton("3");
	protected JButton btn4 = new JButton("4");
	protected JButton btn5 = new JButton("5");
	protected JButton btn6 = new JButton("6");
	protected JButton btn7 = new JButton("7");
	protected JButton btn8 = new JButton("8");
	protected JButton btn9 = new JButton("9");
	protected JButton btnAdicao = new JButton("+");
	protected JButton btnSubtracao = new JButton("-");
	protected JButton btnMultiplicacao = new JButton("*");
	protected JButton btnDivisao = new JButton("/");
	protected JButton btnLimpar = new JButton("C");
	protected JButton btnIgual = new JButton("=");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Janela window = new Janela();
					window.frmCalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Janela() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculadora = new JFrame();
		frmCalculadora.setTitle("Calculadora");
		frmCalculadora.setBounds(100, 100, 322, 406);
		frmCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		lblVisor.setHorizontalAlignment(SwingConstants.RIGHT);
		lblVisor.setFont(new Font("Tahoma", Font.BOLD, 41));
		frmCalculadora.getContentPane().add(lblVisor, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		frmCalculadora.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(4, 4, 0, 0));
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateDigito (((JButton)e.getSource()).getText().charAt(0));
			}
		});
		
		btn7.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btn7);
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateDigito (((JButton)e.getSource()).getText().charAt(0));
			}
		});
		
		btn8.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btn8);
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateDigito (((JButton)e.getSource()).getText().charAt(0));
			}
		});
		
		btn9.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btn9);
		btnAdicao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateOperador('+');
			}
		});
		
		btnAdicao.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btnAdicao);
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateDigito (((JButton)e.getSource()).getText().charAt(0));
			}
		});
		
		btn4.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btn4);
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateDigito (((JButton)e.getSource()).getText().charAt(0));
			}
		});
		
		btn5.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btn5);
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateDigito (((JButton)e.getSource()).getText().charAt(0));
			}
		});
		
		btn6.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btn6);
		btnSubtracao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateOperador('-');
			}
		});
		
		btnSubtracao.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btnSubtracao);
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateDigito (((JButton)e.getSource()).getText().charAt(0));
			}
		});
		
		btn1.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btn1);
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateDigito (((JButton)e.getSource()).getText().charAt(0));
			}
		});
		
		btn2.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btn2);
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateDigito (((JButton)e.getSource()).getText().charAt(0));
			}
		});
		
		btn3.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btn3);
		btnMultiplicacao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateOperador('*');
			}
		});
		
		btnMultiplicacao.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btnMultiplicacao);
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateDigito (((JButton)e.getSource()).getText().charAt(0));
			}
		});
		
		btn0.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btn0);
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				trateLimpar();
			}
		});
		
		btnLimpar.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btnLimpar);
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateIgual();
			}
		});
		
		btnIgual.setFont(new Font("Tahoma", Font.BOLD, 30));
		panel.add(btnIgual);
		btnDivisao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				trateOperador('/');
			}
		});
		
		btnDivisao.setFont(new Font("Tahoma", Font.PLAIN, 30));
		panel.add(btnDivisao);
	}
	
	protected void trateDigito (char d)
	{
		
		if(!(zeroEsquerda(d)))
		{	
		    if (acrescenta)
		        lblVisor.setText (lblVisor.getText()+d);
		    else
		    {
		        lblVisor.setText (""+d);
		        acrescenta = true;
		    }
		}
	}
	protected boolean zeroEsquerda(char d)
	{
		if(lblVisor.getText() == "0" && d == '0')
			return true;
		
		return false;
	}
	
	protected void trateOperador(char opr)
	{
		this.operando1 = Double.parseDouble(lblVisor.getText());
		this.operador = opr;
		this.acrescenta = false;
		
	}
	protected void trateIgual()
	{
		if(this.operador != ' ')
		{
			double operando2 = Double.parseDouble(lblVisor.getText());
			switch(this.operador)
			{
				case '+': lblVisor.setText(""+(this.operando1 + operando2));break;
				case '-': lblVisor.setText(""+(this.operando1 +operando2));break;
				case '*': lblVisor.setText(""+(this.operando1 +operando2));break;
				case '/': lblVisor.setText(""+(this.operando1 +operando2));break;
			}
		}
		
		this.acrescenta = false;
		this.operador = ' ';
	}
	protected void trateLimpar()
	{
		lblVisor.setText("0");
		this.operador = ' ';
		this.acrescenta = false;
	}
}
